﻿using System.Collections;

namespace KPZLab4.Class
{
    public partial class Matrix : IEnumerable
    {
        public int N { get; private set; }
        public int M { get; private set; }
        private double[,]? _matrix;
        private int counter = 0;

        public double GetDeterminant => CalculateDeterminant(new CancellationTokenSource().Token).Result;
        public bool IsMatrixQuadratic => N == M;
        public bool IsDeterminantZero => GetDeterminant == 0;
        public int Rank => _matrix != null ? _matrix.Rank : 0;
        public double this[int i, int j]
        {
            get { return _matrix is not null ? _matrix[i, j] : 0D; }
            set
            {
                if (_matrix is null)
                    return;
                _matrix[i, j] = value;
            }
        }

        public Matrix() : this(0, 0) { }
        public Matrix(int N) : this(N, N) { }
        public Matrix(int n, int m)
        {
            (N, M, _matrix) = (n, m, new double[n, m]);
        }
        public Matrix(double[,] matrix) : this(matrix.GetLength(0), matrix.GetLength(1))
        {
            _matrix = matrix;
        }
        public Matrix(Matrix matrix) : this(matrix._matrix ?? new double[0, 0]) { }
        public void Add(params double[] value)
        {
            for (int j = 0; j < value.Length; j++)
            {
                try { this[counter, j] = value[j]; }
                catch (IndexOutOfRangeException) { throw new IndexOutOfRangeException("Не верный размер матрицы!"); }
            }
            ++counter;
        }
        /// <exception cref="ArgumentNullException"></exception>
        public Matrix Transpose()
        {
            if (_matrix is null)
                throw new ArgumentNullException("Матрица пуста");

            Matrix tmp = new(M, N);

            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                    tmp[j, i] = _matrix[i, j];
            return tmp;
        }
        /// <exception cref="ArgumentException"></exception>
        public Matrix GetReverseMatrix()
        {
            if (IsDeterminantZero)
                throw new ArgumentException("Выродженая матрица, нахождение обратной неопределено!");
            if (!IsMatrixQuadratic)
                throw new ArgumentException("Не квадратная матрица!");
            
            Matrix det_matrix = new(N, M);
            var Token = new CancellationTokenSource().Token;

            for (int i = 0; i < N; i++)
                for (int j = 0; j < M; j++)
                    det_matrix[i, j] = Math.Pow(-1D, i + j) * CalculateDeterminant(Token, CalculateMinor(i, j)).Result;
            
            return 1D / GetDeterminant * det_matrix.Transpose();
        }      
        public double Trace()
        {
            double tmp = 0D;
            for (int i = 0; i < N; i++)
                tmp += this[i, i];
            return tmp;
        }
        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(this, obj))
                return true;
            if (obj is null)
                return false;
            return _matrix != null && _matrix.Equals(obj);
        }
        public override int GetHashCode()
        {
            return _matrix != null ? _matrix.GetHashCode() : 0;
        }
        /// <exception cref="ArgumentNullException"></exception>
        public IEnumerator GetEnumerator()
        {
            if (_matrix is null)
                throw new ArgumentNullException();
            return _matrix.GetEnumerator();
        }
        public override string ToString()
        {
            string tmp = "";
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                    tmp += this[i, j] + " ";
                tmp += " \r\n";
            }
            return tmp;
        }
    }
    public partial class Matrix : ICloneable
    {
        /// <exception cref="ArithmeticException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public static Matrix operator ^(Matrix matrix, int pow)
        {
            if (!matrix.IsMatrixQuadratic)
                throw new ArithmeticException("N != M");
            if (matrix._matrix is null)
                throw new ArgumentNullException("Матрица пуста!", new Exception());
            Matrix tmp = new(matrix);
            for (int i = 0; i < pow; i++)
                tmp *= matrix;
            return tmp;
        }
        /// <exception cref="ArithmeticException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public static Matrix operator *(Matrix left, Matrix right)
        {
            if (left.M != right.N)
                throw new ArithmeticException("N != M");
            if (left._matrix is null || right._matrix is null)
                throw new ArgumentNullException("Матрица пуста!");

            Matrix matrixC = new(left.N, right.M);

            for (int i = 0; i < left.N; i++)
                for (int j = 0; j < right.M; j++)
                    for (int k = 0; k < left.M; k++)
                        matrixC[i, j] += left[i, k] * right[k, j];
            return matrixC;
        }
        /// <summary>
        /// Умножение матрицы на скаляр.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Matrix operator *(double left, Matrix right)
        {
            Matrix tmp = right;
            for (int i = 0; i < right.N; i++)
                for (int j = 0; j < right.M; j++)
                    tmp[i, j] *= left;
            return tmp;
        }
        /// <summary>
        /// Умножение матрицы на скаляр.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Matrix operator *(Matrix left, double right)
        {
            return right * left;
        }
        /// <summary>
        /// Заметка:
        /// Все свойства линейных операций повторяют аксиомы линейного пространства.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public static Matrix operator +(Matrix left, Matrix right)
        {
            if ((left.N != right.N) || (left.M != right.M))
                throw new ArgumentException("Матрицы должны быть одинакового размера!");
            if ((left._matrix is null) || (right._matrix is null))
                throw new ArgumentNullException("Матрица пуста!");

            Matrix tmp = new(left.N, right.M);

            for (int i = 0; i < left.N; i++)
                for (int j = 0; j < right.M; j++)
                    tmp[i, j] += left._matrix[i, j] + right._matrix[i, j];

            return tmp;
        }
        public static Matrix operator -(Matrix left, Matrix right)
        {
            return left + (right * -1D);
        }
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public static Matrix operator /(Matrix left, Matrix right)
        {
            if (left.IsMatrixQuadratic != right.IsMatrixQuadratic)
                throw new ArgumentException("Матрицы не квадартные");
            if (left.IsDeterminantZero || right.IsDeterminantZero)
                throw new ArgumentException("Определитель равен нулю");
            return left * right.GetReverseMatrix();
        }
        public static bool operator ==(Matrix left, Matrix right)
        {
            if ((left.N != right.N) || (left.M != right.M))
                return false;
            for (int i = 0; i < left.N; i++)
                for (int j = 0; j < right.M; j++)
                    if (left[i, j] != right[i, j]) 
                        return false;
            return true;
        }
        public static bool operator !=(Matrix left, Matrix right)
        {
            return !(left == right);
        }
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
    public partial class Matrix
    {
        private Matrix CalculateMinor(int x0, int y0)
        {
            if (x0 >= N || y0 >= M || _matrix is null)
                return new();

            Matrix minor = new(N - 1, M - 1);
            byte xOffset = 0,
                 yOffset = 0;

            for (int x = 0; x < N; x++)
            {
                if (x == x0)
                {
                    xOffset = 1;
                    continue;
                }
                for (int y = 0; y < M; y++)
                {
                    if (y == y0)
                    {
                        yOffset = 1;
                        continue;
                    }
                    minor[x - xOffset, y - yOffset] = _matrix[x, y];
                }
                yOffset = 0;
            }
            return minor;
        }
        private async Task<double> CalculateDeterminant(CancellationToken ct)
        {
            if (N != M || _matrix is null)
                return double.NaN;
            if (N == 1)
                return _matrix[0, 0];
            if (N == 2)
                return _matrix[0, 0] * _matrix[1, 1] - _matrix[0, 1] * _matrix[1, 0];

            ct.ThrowIfCancellationRequested();
            await Task.Delay(1, ct);
            double determinant = 0;

            for (int i = 0; i < N; i++)
            {
                double m = _matrix[i, 0];
                if (m != 0)
                    determinant += m * (await CalculateMinor(i, 0).CalculateDeterminant(ct)) * (((2 + i) & 1) is 0 ? 1 : -1);
            }
            return determinant;
        }
        private static async Task<double> CalculateDeterminant(CancellationToken ct, Matrix minor)
        {
            return await minor.CalculateDeterminant(ct);
        }
    }
}
