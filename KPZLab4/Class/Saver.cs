﻿using System.Text.RegularExpressions;

namespace KPZLab4.Class
{
    internal class Saver
    {
        #region Singleton_Realization
        private static Saver? instance;
        public string? GetSrc { get; }
        public bool IsExist { get; }
        public bool IsEmpry => GetSrc != null && new FileInfo(GetSrc).Length == 0;

        private Saver(string src)
        {
            (GetSrc, IsExist) = (src, File.Exists(src));
        }
        public static Saver Instance(string src)
        {
            return instance ??= new(src);
        }
        #endregion Singleton_Realization

        public int CountMatrix => new Regex(@"#region\s{1,}matrix\s{1,}\d\s*")
                                     .Matches(File.ReadAllText(instance?.GetSrc ?? throw new ArgumentException("Путь указывает на не существующий файл")))
                                     .Count;
        
        public static List<string> GetLinesFromFile()
        {
            return File.ReadLines(instance?.GetSrc ?? throw new ArgumentException("Путь указывает на не существующий файл")).ToList();
        }
       
        public void Defragmentation()
        {
            if (GetSrc == null) 
                throw new ArgumentException("Путь указывает на не существующий файл");
            File.WriteAllLines(GetSrc, File.ReadAllLines(GetSrc).Where(arg => !string.IsNullOrWhiteSpace(arg)));
        }
        public void Clear()
        {
            if (GetSrc == null)
                throw new ArgumentException("Путь указывает на не существующий файл");
            StreamWriter fileStream = new (GetSrc, false);
            fileStream.Dispose();
            fileStream.Close();
        }
        public List<double[]> GetLinesFromFile(int matrix_id)
        {
            if (GetSrc == null)
                throw new ArgumentException("Путь указывает на не существующий файл");
            else
            {
                string _global_search_pattern = "(#region\\s{1}matrix\\s{1}"
                                                   + matrix_id
                                                   + "\\s*(-|\\d+\\s*){1,}#endregion\\s{1}matrix\\s{1}"
                                                   + matrix_id + "\\s*)";
                string _search_current_matrix_pattern = @"\n(-|\d+\s*){1,}";
                string _get_double_values_pattern = @"\n(-|\d+ ){1,}";

                return Regex.Matches(
                   FileSaver.FindMatrix(_search_current_matrix_pattern,
                              FileSaver.FindMatrix(_global_search_pattern,
                                                   File.ReadAllText(GetSrc)
                                                  )
                                       ), _get_double_values_pattern
                                    )
                            .Select(x => x.Value
                                          .Split(" ", StringSplitOptions.RemoveEmptyEntries)
                                          .Select(y => double.Parse(y))
                                          .ToArray()
                                   ).ToList();
            }
        }

        public Matrix ToMatrix(int matrix_id)
        {
            List<double[]> double_arrays = GetLinesFromFile(matrix_id);
            Matrix matrix = new(double_arrays.Count, double_arrays[0].Length);
            double_arrays.ForEach(matrix.Add);
            return matrix;                
        }
    }

    internal static class FileSaver
    {
        public static string FindMatrix(string pattern, string value)
        {
            return new Regex(pattern).Match(value).Value;
        }
        public static void WriteArray(this Matrix myArray, Saver? instance, bool IsNotNewFile)
        {
            if (instance?.GetSrc == null)
                throw new ArgumentException("Путь указывает на не существующий файл");

            int id = instance.CountMatrix;
            StreamWriter streamWriter = new(instance.GetSrc, IsNotNewFile);
            streamWriter.WriteLine(
                                   "#region matrix " + id + "\r\n"
                                   + myArray.ToString()
                                   + "#endregion matrix " + id + "\n\r\n"
                                   );
            streamWriter.Close();
        }

        public static void ReplaceIf(this Matrix current_matrix, Saver? instance, int matrix_id)
        {
            Saver src;
            if (string.IsNullOrEmpty(instance?.GetSrc) || instance == null)
                throw new ArgumentException("Путь указывает на не существующий файл");
            src = instance;
            if (src.IsEmpry)
                current_matrix.WriteArray(src, false);
            else
            {
                var tmp_matrix = src.ToMatrix(matrix_id);

                if (tmp_matrix?.N != current_matrix.M)
                    current_matrix.WriteArray(src, true);
                else
                {
                    string file_items = File.ReadAllText(src.GetSrc);
                    string tmp =
                    file_items.Replace(
                                   FindMatrix(
                                              "(#region\\s{1}matrix\\s{1}" + matrix_id
                                              + "\\s*(-|\\d+\\s*){1,}"
                                              + "#endregion\\s{1}matrix\\s{1}" + matrix_id + "\\s*)",
                                              file_items
                                             ),
                                              "#region matrix " + matrix_id + "\r\n"
                                              + current_matrix.ToString()
                                              + "#endregion matrix " + matrix_id + "\n\r\n"
                                      );
                    File.WriteAllText(src.GetSrc, tmp);
                }
            }
        }
    }
}
