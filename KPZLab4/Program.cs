﻿using KPZLab4.Class;
using System;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Text.RegularExpressions;

namespace KPZLab4
{
    public class Calculator<T> where T : struct
    {
        static Calculator()
        {
            if (!new[]
                {
                    typeof(int),
                    typeof(long),
                    typeof(float),
                    typeof(double),
                    typeof(decimal)
            }.Contains(typeof(T)))
                throw new NotSupportedException($"Type `{typeof(T).FullName}` isn't supported!");
        }
        public T Add(T A, T B) => (new Vector<T>(A) + new Vector<T>(B))[0];
    }

    class Program
    {

        public static void Print(Matrix matrix)
        {
            for (int i = 0; i < matrix.N; i++)
            {
                for (int j = 0; j < matrix.M; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public static List<double[]> GetLinesFromFile(int matrix_id)
        {
            return Regex.Matches(
                   FindRegexString(@"\n(-|\d+\s*){1,}",
                                   FindRegexString("(#region\\s{1}matrix\\s{1}" + matrix_id + "\\s*(-|\\d+\\s*){1,}#endregion\\s{1}matrix\\s{1}" + matrix_id + "\\s*)",
                                                   File.ReadAllText("C:\\Users\\1\\source\\repos\\KPZLab4\\Matrix.txt")
                                                   )
                                  ), @"\n(-|\d+ ){1,}"
                                )
                  .Select(x => x.Value
                                .Split(" ", StringSplitOptions.RemoveEmptyEntries)
                                .Select(y => double.Parse(y))
                                .ToArray()
                         ).ToList();
        }


        //public static Matrix ToMatrix(int matrix_id)
        //{
        //    List<string[]> strings = GetLinesFromFile(matrix_id);
        //    double[] doubles = new double[strings[0].Length];
        //    Matrix matrix = new(strings.Count, strings[0].Length);

        //    foreach (var item in strings)
        //    {
        //        for (int i = 0; i < doubles.Length; i++)
        //            doubles[i] = double.Parse(item[i]);
        //        matrix.Add(doubles);
        //    }
        //    return matrix;
        //}

        public static string FindRegexString(string pattern, string value) => new Regex(pattern).Match(value).Value;

        public static void Main()
        {
            Saver file = Saver.Instance("C:\\Users\\dro1l\\OneDrive\\Desktop\\ztu\\kpz\\kpzlab4\\KPZLab4\\Matrix.txt");

            Matrix matrix = new(3, 3)
            {
                { 1, 2 ,4 },
                { 0, 2, 1 },
                { 3, 2 , 1 }
            };

            //Saver.WriteArray(matrix, false);

            file.Clear();

            //Console.WriteLine(Saver.CountMatrix);

            FileSaver.WriteArray(matrix, file, true);

          
        }
    }
}
